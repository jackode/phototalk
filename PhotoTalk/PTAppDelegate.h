//
//  PTAppDelegate.h
//  PhotoTalk
//
//  Created by Admin on 1/22/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
