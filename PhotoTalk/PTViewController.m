//
//  PTViewController.m
//  PhotoTalk
//
//  Created by Admin on 1/22/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "PTViewController.h"
#import "IAPickerController.h"
@interface PTViewController ()
<UINavigationControllerDelegate, IAPickerControllerDelegate>
@end

@implementation PTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

//------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------------------------------------------------------------------------------
- (IBAction)pickImageNSound:(id)sender
{
    IAPickerController* iaPickerController = [[IAPickerController alloc] init];
    [iaPickerController setAudioSourceType:(IAPickerSourceTypeMic)];
    [iaPickerController setPhotoSourceType:(IAPickerSourceTypeCamera)];
    
    
    [iaPickerController setCameraDevice:(IAPickerCameraDeviceCameraDeviceRear)];
    [iaPickerController setImageQuality:(IAPickerQualityTypeMedium)];
    
    [iaPickerController setAllowsAudioEditing:YES];
    iaPickerController.delegate = self;
    
    [self setModalPresentationStyle:(UIModalPresentationFullScreen)];
    [self presentViewController:iaPickerController
                       animated:YES
                     completion:nil];

}


#pragma mark IAPickerController delegate
- (void) iaPicker: (IAPickerController*) picker
didCancelWithError: (NSError*) error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
