//
//  main.m
//  PhotoTalk
//
//  Created by Admin on 1/22/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PTAppDelegate class]));
    }
}
