//
//  IACompoundPickerViewController.m
//  PhotoTalk
//
//  Created by Admin on 1/29/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "IACompoundPickerViewController.h"
#import "IAProtocols.h"
#import <AVFoundation/AVFoundation.h>
#import "IACaptureManager.h"
#import "IAProtocols.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "DDExpandableButton.h"

#define kCompoundPickerCapButtonH 60
#define kCompoundPickerCapButtonW 60
#define kUpdateFrequency	60.0
@interface IACompoundPickerViewController ()
<AVAudioRecorderDelegate,
IACaptureManagerDelegate,
AVAudioPlayerDelegate, UIAccelerometerDelegate>
{
    id<IAMediaPickerDelegate> _delegate;
    
    AVAudioRecorder* _audioRecorder;
    UIView* _cameraPreviewView;
    AVCaptureVideoPreviewLayer* _videoPreviewLayer;
    IACaptureManager* _captureManager;
    
    NSString* _tempPathAudio;
    
    UIButton* _captureButton;
    UIAcceleration* _lastAcceleration;
    DDExpandableButton *torchModeButton;
}
@end

@implementation IACompoundPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//------------------------------------------------------------------------------
- (id) initWithCameraType:(IAPickerCameraDevice)deviceType
             imageQuality:(IAPickerQualityType)imageQuality
            audioDuration:(NSTimeInterval)audioDuration
                 delegate:(id<IAMediaPickerDelegate>)delegate
{
    self = [super init];
    if (self) {
        // Init view controller here
        // TODO: init here
        /*
         1. Init capture session to take picture from camera
         2. Setup camera properties
         3. Setup audio recoder
         */
        
        _delegate = delegate;
    }
    return self;
}
- (void) viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
//------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [self setTitle:@"Camera"];
    UIButton* recordButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [recordButton setFrame:CGRectMake(0, 0, kCompoundPickerCapButtonW,
                                      kCompoundPickerCapButtonH)];
    [recordButton setImage:[UIImage imageNamed:@"record_shine.png"]
                  forState:(UIControlStateNormal)];
    
    [recordButton setCenter:CGPointMake(self.view.frame.size.width/2,
                self.view.frame.size.height-kCompoundPickerCapButtonH/2-10)];
    
    [recordButton setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin |
                                       UIViewAutoresizingFlexibleRightMargin |
                                       UIViewAutoresizingFlexibleTopMargin)];
    [self.view addSubview:recordButton];
    
    [recordButton addTarget:self
                     action:@selector(startRecord:)
           forControlEvents:(UIControlEventTouchDown)];
    
    [recordButton addTarget:self
                     action:@selector(finishRecordAndCapture:)
           forControlEvents:(UIControlEventTouchUpInside)];
    
    [recordButton addTarget:self
                     action:@selector(cancelRecord:)
           forControlEvents:(UIControlEventTouchUpOutside)];
    
    
    _captureButton = recordButton;

    

    
    // TODO: need to better name convention for temporary recorded audio
    if (!_tempPathAudio) {
        _tempPathAudio = [NSTemporaryDirectory()
                          stringByAppendingPathComponent:@"phototalk.caf"];
    }
    
    if (([self audioRecorder] == nil) ||
        ([self captureManager] == nil))
    {
        [UIAlertView
         alertViewWithTitle:@""
         message:@"Can not find capture devices" cancelButtonTitle:@"Dismiss"
         onCancel:^{
             // TODO: Need to define error code
             if ([_delegate
                  respondsToSelector:@selector(picker:failedWithError:)])
             {
                 NSError* error =
                 [NSError errorWithDomain:@"IACompoundPickerViewController"
                                     code:-1
                                 userInfo:[NSDictionary
                                           dictionaryWithObject:@"Cannot init"
                                           forKey:@"description"]];
                 
                 [_delegate picker:self failedWithError:error];
             }
             
         } ];
        
        return;
    }
    
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0 / kUpdateFrequency];
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
    
    [self.view bringSubviewToFront:_captureButton];
    
    torchModeButton =
    [[DDExpandableButton alloc]
     initWithPoint:CGPointMake(20.0f, 20.0f)
     leftTitle:[UIImage imageNamed:@"Flash.png"]
     buttons:[NSArray arrayWithObjects:@"Auto",@"On", @"Off" , nil]];
   
    [self.view addSubview:torchModeButton];
    
    [torchModeButton addTarget:self
                        action:@selector(toggleFlashlight:)
              forControlEvents:UIControlEventValueChanged];
    [torchModeButton setVerticalPadding:6];
    [torchModeButton updateDisplay];
    [torchModeButton setSelectedItem:2];
    
    
    DDExpandableButton *toggleCameraButton = [[DDExpandableButton alloc]
                                        initWithPoint:CGPointMake(240.0f, 20.0f)
                                        leftTitle:[UIImage imageNamed:@"camera.png"]
                                        buttons:[NSArray arrayWithObjects:
                                                 @"        ",
                                                 @" ", nil]];
    
    [toggleCameraButton setToggleMode:YES];
    [toggleCameraButton setInnerBorderWidth:0];
	[toggleCameraButton setHorizontalPadding:6];
    [toggleCameraButton setSelectedItem:1]; //default to rear camera
	[toggleCameraButton updateDisplay];
    [toggleCameraButton addTarget:self
                           action:@selector(toggleCamera:)
                 forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:toggleCameraButton];
}


//------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//------------------------------------------------------------------------------
#pragma mark Utilities
- (AVAudioRecorder*) audioRecorder
{
    
    if (_audioRecorder) {
        return _audioRecorder;
    }
    
    NSError* error = nil;
    
    NSDictionary *recordSettings =
    [NSDictionary
     dictionaryWithObjectsAndKeys:
     [NSNumber numberWithInt:AVAudioQualityMin], AVEncoderAudioQualityKey,
     [NSNumber numberWithInt:16], AVEncoderBitRateKey,
     [NSNumber numberWithInt: 2], AVNumberOfChannelsKey,
     [NSNumber numberWithFloat:44100.0], AVSampleRateKey,  nil];
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:[NSURL fileURLWithPath:_tempPathAudio]
                      settings:recordSettings
                      error:&error];
    
    if (error) {
        _audioRecorder = nil;
        return _audioRecorder;
    }
    
    
    if (![_audioRecorder prepareToRecord]) {
        _audioRecorder = nil;
        return _audioRecorder;
    }
    
    [_audioRecorder setDelegate:self];
    return _audioRecorder;
}

//------------------------------------------------------------------------------
- (IACaptureManager*) captureManager
{
    if (_captureManager) {
        return  _captureManager;
    }
    else
    {
        IACaptureManager* manager = [[IACaptureManager alloc] init];
        [manager setDelegate:self];
        _captureManager = manager;
        
        if ([_captureManager setupSession]) {
            
            AVCaptureVideoPreviewLayer* capturePreviewLayer =
            [[AVCaptureVideoPreviewLayer alloc]
             initWithSession:[_captureManager session]];
            
            _cameraPreviewView = [[UIView alloc ]
                                  initWithFrame:[self.view bounds]];
            
            [self.view addSubview:_cameraPreviewView];
            [_cameraPreviewView
             setAutoresizingMask:(UIViewAutoresizingFlexibleWidth |
                                  UIViewAutoresizingFlexibleHeight)];
            
            CALayer* viewLayer = [_cameraPreviewView layer];
            [viewLayer setMasksToBounds:YES];
            
            CGRect bounds = [_cameraPreviewView bounds];
            [capturePreviewLayer setFrame:bounds];
            [capturePreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            
            [viewLayer insertSublayer:capturePreviewLayer
                                below:[[viewLayer sublayers]objectAtIndex:0]];
            
            _videoPreviewLayer = capturePreviewLayer;
            
            // Start capture session
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[[_captureManager session] startRunning];
			});
            return _captureManager;
        }
        else
        {
            _captureManager = nil;
            return _captureManager;
        }
    }
}
//------------------------------------------------------------------------------
#pragma mark Handle Control Events
- (void) startRecord:(id) sender
{
    [[self audioRecorder] record];
}

//------------------------------------------------------------------------------
- (void) finishRecordAndCapture: (id) sender
{
    if([[self audioRecorder] isRecording])
        [[self audioRecorder] stop];
    
    [[self captureManager] captureStillImage];
}

//------------------------------------------------------------------------------
- (void) cancelRecord: (id) sender
{
    if([[self audioRecorder] isRecording])
        [[self audioRecorder] stop];
}

//------------------------------------------------------------------------------
- (void)toggleFlashlight:(DDExpandableButton *)sender
{
    if (_captureManager.videoInput)
	{
        
		AVCaptureDevice *device = [_captureManager.videoInput device];		
		if ([device hasTorch])
		{
			[device lockForConfiguration:nil];
			[device setTorchMode:(2 - [sender selectedItem])];
			[device unlockForConfiguration];
		}
	}
}
//------------------------------------------------------------------------------
- (void)toggleCamera:(DDExpandableButton *)sender
{
    [_captureManager toggleCamera];
    if ([[_captureManager.videoInput device] position] != AVCaptureDevicePositionBack)
       [torchModeButton setHidden:YES];
    else
        [torchModeButton setHidden:NO];
}
//------------------------------------------------------------------------------
#pragma mark AVAudioRecorderDelegate
- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder
                            successfully:(BOOL)flag
{
    NSLog(@"Successfully record to file: %@", [[recorder url] absoluteString]);

}
//------------------------------------------------------------------------------
- (void) audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder
                                    error:(NSError *)error
{
    NSLog(@"Record audio error: %@", error);
    if ([_delegate respondsToSelector:@selector(picker:failedWithError:)])
    {
        [_delegate picker:self failedWithError:error];
    }
}

//------------------------------------------------------------------------------
#pragma mark IACaptureManagerDelegate
- (void) captureManager:(IACaptureManager *)captureManager
       didFailWithError:(NSError *)error
{
    
    if ([_delegate respondsToSelector:@selector(picker:failedWithError:)])
    {
        [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
        [_delegate picker:self failedWithError:error];
    }
}
//------------------------------------------------------------------------------
- (void) captureManager: (IACaptureManager *)captureManager
      captureStillImage: (UIImage*) image
{

    if ([_delegate
         respondsToSelector:@selector(picker:didPickedImage:audioPath:acceleration:)])
    {
        [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
        [_delegate picker:self
           didPickedImage:image
                audioPath:_tempPathAudio
             acceleration:_lastAcceleration];
    }
}

#pragma mark Accelerometer Delegate
// UIAccelerometerDelegate method, called when the device accelerates.
-(void)accelerometer:(UIAccelerometer *)accelerometer
       didAccelerate:(UIAcceleration *)acceleration
{
    _lastAcceleration = acceleration;
	// Update the accelerometer graph view
//	if(!isPaused)
//	{
//		[filter addAcceleration:acceleration];
//		[unfiltered addX:acceleration.x y:acceleration.y z:acceleration.z];
//		[filtered addX:filter.x y:filter.y z:filter.z];
//	}
}

@end
