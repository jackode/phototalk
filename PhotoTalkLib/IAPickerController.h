//
//  IAViewController.h
//  PhotoTalk
//
//  Created by Admin on 1/22/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//


/*!
 This class provide convenient class to pick an image along with a sound.
 */
#import <UIKit/UIKit.h>

@protocol IAPickerControllerDelegate;

typedef NS_ENUM(NSInteger, IAPickerSourceType) {
    IAPickerSourceTypePhotoLibrary,
    IAPickerSourceTypeCamera,
//    IAPickerSourceTypeSavedPhotosAlbum,
    IAPickerSourceTypeAudioLibrary,
    IAPickerSourceTypeMic,
//    IAPickerSourceTypeSavedAudioAlbum,
};

typedef NS_ENUM(NSInteger, IAPickerCameraDevice) {
    IAPickerCameraDeviceCameraDeviceRear,
    IAPickerCameraDeviceCameraDeviceFront
};

typedef NS_ENUM(NSInteger, IAPickerQualityType) {
    IAPickerQualityTypeHigh = 0,       // highest quality
    IAPickerQualityTypeMedium = 1,     // medium quality, suitable for transmission via Wi-Fi
    IAPickerQualityTypeLow = 2,        // lowest quality, suitable for tranmission via cellular network
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_0
    IAPickerQualityType640x480 = 3,    // VGA quality
#endif
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_5_0
    IAPickerQualityTypeIFrame1280x720 = 4,
    IAPickerQualityTypeIFrame960x540 = 5
#endif
};

typedef NS_ENUM(NSInteger, IAPickerCameraCaptureMode) {
    IAPickerCameraCaptureModePhoto,
    IAPickerCameraCaptureModeVideo
};

typedef NS_ENUM(NSInteger, IAPickerCameraFlashMode) {
    IAPickerCameraFlashModeOff  = -1,
    IAPickerCameraFlashModeAuto = 0,
    IAPickerCameraFlashModeOn   = 1
};


// info dictionary keys
//UIKIT_EXTERN NSString *const UIImagePickerControllerMediaType;      // an NSString (UTI, i.e. kUTTypeImage)
//UIKIT_EXTERN NSString *const UIImagePickerControllerOriginalImage;  // a UIImage
//UIKIT_EXTERN NSString *const UIImagePickerControllerEditedImage;    // a UIImage
//UIKIT_EXTERN NSString *const UIImagePickerControllerCropRect;       // an NSValue (CGRect)
//UIKIT_EXTERN NSString *const UIImagePickerControllerMediaURL;       // an NSURL
//UIKIT_EXTERN NSString *const UIImagePickerControllerReferenceURL        NS_AVAILABLE_IOS(4_1);  // an NSURL that references an asset in the AssetsLibrary framework
//UIKIT_EXTERN NSString *const UIImagePickerControllerMediaMetadata       NS_AVAILABLE_IOS(4_1);  // an NSDictionary containing metadata from a captured photo


//------------------------------------------------------------------------------
@interface IAPickerController : UINavigationController
{
@private
    // Some private ivariable
    
}

+ (BOOL) isSourceTypeAvailable:(IAPickerSourceType) sourceType;
+ (NSArray*) availableMediaTypesforSourceType:(IAPickerSourceType) sourceType;

+ (BOOL)isCameraDeviceAvailable:(IAPickerCameraDevice)cameraDevice;
+ (BOOL)isFlashAvailableForCameraDevice:(IAPickerCameraDevice)cameraDevice;
+ (NSArray *)availableCaptureModesForCameraDevice:(IAPickerCameraDevice)cameraDevice;


@property(nonatomic,assign)
id <UINavigationControllerDelegate, IAPickerControllerDelegate> delegate;

@property(nonatomic) IAPickerSourceType photoSourceType;
@property(nonatomic) IAPickerSourceType audioSourceType;

@property(nonatomic) BOOL allowsAudioEditing;
@property(nonatomic) BOOL allowsImageEditing;

@property(nonatomic) IAPickerQualityType imageQuality;
@property(nonatomic) NSTimeInterval  audioMaximumDuration; // in seconds


//@property(nonatomic)           BOOL                                  showsCameraControls;
@property(nonatomic,retain)  UIView *cameraOverlayView;
@property(nonatomic) CGAffineTransform cameraViewTransform;

@property(nonatomic) IAPickerCameraDevice cameraDevice;
@property(nonatomic) IAPickerCameraFlashMode cameraFlashMode;


- (void) takePicture;
- (BOOL) startAudioRecord;
- (void) stopAudioRecord;
- (void) startTakeImageAudio;

@end

//------------------------------------------------------------------------------
@protocol IAPickerControllerDelegate<NSObject>
@optional
// The picker does not dismiss itself; the client dismisses it in these callbacks.
// The delegate will receive one or the other, but not both, depending whether the user
// confirms or cancels.
//-       (void)IAPickerController:(UIImagePickerController *)picker
//           didFinishPickingImage:(UIImage *)image
//                     editingInfo:(NSDictionary *)editingInfo;// NS_DEPRECATED_IOS(2_0, 3_0);
//
//- (void)imagePickerController:(UIImagePickerController *)picker
//didFinishPickingMediaWithInfo:(NSDictionary *)info;
//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;

- (void) iaPicker: (IAPickerController*) picker didCancelWithError: (NSError*) error;
@end

