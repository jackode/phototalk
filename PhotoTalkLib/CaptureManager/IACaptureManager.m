//
//  IACaptureManager.m
//  avtest
//
//  Created by Admin on 2/5/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "IACaptureManager.h"

@interface IACaptureManager (InternalUtility)
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition)position;
- (AVCaptureDevice *) frontFacingCamera;
- (AVCaptureDevice *) backFacingCamera;
- (AVCaptureDevice *) audioDevice;
- (void)deviceOrientationDidChange;
@end

//==============================================================================
@implementation IACaptureManager
@synthesize session=_session;
@synthesize orientation=_orientation;
@synthesize videoInput=_videoInput;
@synthesize audioInput=_audioInput;
@synthesize stillImageOutput=_stillImageOutput;

@synthesize deviceConnectedObserver;
@synthesize deviceDisconnectedObserver;
@synthesize backgroundRecordingID;
@synthesize delegate;

//------------------------------------------------------------------------------
//- (void) setFlashMode:(AVCaptureFlashMode)aflashMode{
//    [[_videoInput device] setTorchMode:aflashMode];
//}
//
//- (AVCaptureFlashMode) flashMode
//{
//    return [[_videoInput device] torchMode];
//}

//------------------------------------------------------------------------------
- (id) init
{
    self = [super init];
    if (self != nil) {
		__block id weakSelf = self;
        
        void (^deviceConnectedBlock)(NSNotification *) =
        ^(NSNotification *notification) {
			AVCaptureDevice *device = [notification object];
			
			BOOL sessionHasDeviceWithMatchingMediaType = NO;
            
			NSString *deviceMediaType = nil;
            
			if ([device hasMediaType:AVMediaTypeAudio])
                deviceMediaType = AVMediaTypeAudio;
			else if ([device hasMediaType:AVMediaTypeVideo])
                deviceMediaType = AVMediaTypeVideo;
			
			if (deviceMediaType != nil) {
				for (AVCaptureDeviceInput *input in [_session inputs]){
					if ([[input device] hasMediaType:deviceMediaType]) {
						sessionHasDeviceWithMatchingMediaType = YES;
						break;
					}
				}
				
				if (!sessionHasDeviceWithMatchingMediaType) {
					NSError	*error;
					AVCaptureDeviceInput *input =[AVCaptureDeviceInput
                                                  deviceInputWithDevice:device
                                                  error:&error];
					if ([_session canAddInput:input])
						[_session addInput:input];
				}
			}
            
			if ([delegate respondsToSelector:@selector(captureManagerDeviceConfigurationChanged:)]) {
				[delegate captureManagerDeviceConfigurationChanged:self];
			}
            
        };
        
        void (^deviceDisconnectedBlock)(NSNotification *) =
        ^(NSNotification *notification) {
			AVCaptureDevice *device = [notification object];
			
			if ([device hasMediaType:AVMediaTypeAudio]) {
				[_session removeInput:[weakSelf audioInput]];
				[weakSelf setAudioInput:nil];
			}
			else if ([device hasMediaType:AVMediaTypeVideo]) {
				[_session removeInput:[weakSelf videoInput]];
				[weakSelf setVideoInput:nil];
			}
			
			if ([delegate respondsToSelector:@selector(captureManagerDeviceConfigurationChanged:)]) {
				[delegate captureManagerDeviceConfigurationChanged:self];
			}
        };
        
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [self setDeviceConnectedObserver:
         [notificationCenter
          addObserverForName:AVCaptureDeviceWasConnectedNotification
          object:nil
          queue:nil
          usingBlock:deviceConnectedBlock]];
        
        [self setDeviceDisconnectedObserver:
         [notificationCenter
          addObserverForName:AVCaptureDeviceWasDisconnectedNotification
          object:nil
          queue:nil
          usingBlock:deviceDisconnectedBlock]];
        
		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
		[notificationCenter
         addObserver:self
         selector:@selector(deviceOrientationDidChange)
         name:UIDeviceOrientationDidChangeNotification
         object:nil];
        
		_orientation = AVCaptureVideoOrientationPortrait;
        
    }
    
    return self;
}

//------------------------------------------------------------------------------
-(BOOL) setupSession
{
    BOOL success = NO;
    
    // Use backFacing camera as default
	// Set torch and flash mode to auto
	if ([[self backFacingCamera] hasFlash]) {
		if ([[self backFacingCamera] lockForConfiguration:nil]) {
			if ([[self backFacingCamera] isFlashModeSupported:AVCaptureFlashModeAuto]) {
				[[self backFacingCamera] setFlashMode:AVCaptureFlashModeAuto];
			}
			[[self backFacingCamera] unlockForConfiguration];
		}
	}
	if ([[self backFacingCamera] hasTorch]) {
		if ([[self backFacingCamera] lockForConfiguration:nil]) {
			if ([[self backFacingCamera] isTorchModeSupported:AVCaptureTorchModeAuto]) {
				[[self backFacingCamera] setTorchMode:AVCaptureTorchModeAuto];
			}
			[[self backFacingCamera] unlockForConfiguration];
		}
	}
	
    
    
    // Init the device inputs
    NSError* error= nil;
    AVCaptureDeviceInput *newVideoInput =
    [[AVCaptureDeviceInput alloc] initWithDevice:[self backFacingCamera]
                                           error:&error];
    
    AVCaptureStillImageOutput *newStillImageOutput = nil;
    if (!error) {
        // Setup the still image file output
        newStillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc]
                                        initWithObjectsAndKeys:
                                        AVVideoCodecJPEG, AVVideoCodecKey, nil];
        
        [newStillImageOutput setOutputSettings:outputSettings];        
    }else{
        return NO;
    }
    
    
    error = nil;
    AVCaptureDeviceInput *newAudioInput =
    [[AVCaptureDeviceInput alloc] initWithDevice:[self audioDevice]
                                           error:&error];
    
	if (error) {
        return NO;
    }
    
    // Create session (use default AVCaptureSessionPresetHigh)
    AVCaptureSession *newCaptureSession = [[AVCaptureSession alloc] init];
    
    // Add inputs and output to the capture session
    if ([newCaptureSession canAddInput:newVideoInput]) {
        [newCaptureSession addInput:newVideoInput];
    }
        
    if ([newCaptureSession canAddInput:newAudioInput]) {
        [newCaptureSession addInput:newAudioInput];
    }
    if ([newCaptureSession canAddOutput:newStillImageOutput]) {
        [newCaptureSession addOutput:newStillImageOutput];
    }
    
    [self setStillImageOutput:newStillImageOutput];
    [self setVideoInput:newVideoInput];
    [self setAudioInput:newAudioInput];
    [self setSession:newCaptureSession];
    
	
    // ******* Set up the movie file output **********
    
//    NSURL *outputFileURL = [self tempFileURL];
//    AVCamRecorder *newRecorder =
//    [[AVCamRecorder alloc] initWithSession:[self session]
//                             outputFileURL:outputFileURL];
//    
//    [newRecorder setDelegate:self];
//	
//	// Send an error to the delegate if video recording is unavailable
//	if (![newRecorder recordsVideo] && [newRecorder recordsAudio]) {
//		NSString *localizedDescription = NSLocalizedString(@"Video recording unavailable", @"Video recording unavailable description");
//		NSString *localizedFailureReason = NSLocalizedString(@"Movies recorded on this device will only contain audio. They will be accessible through iTunes file sharing.", @"Video recording unavailable failure reason");
//		NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
//								   localizedDescription, NSLocalizedDescriptionKey,
//								   localizedFailureReason, NSLocalizedFailureReasonErrorKey,
//								   nil];
//		NSError *noVideoError = [NSError errorWithDomain:@"AVCam" code:0 userInfo:errorDict];
//		if ([[self delegate] respondsToSelector:@selector(captureManager:didFailWithError:)]) {
//			[[self delegate] captureManager:self didFailWithError:noVideoError];
//		}
//	}
//	
//	[self setRecorder:newRecorder];
//    [newRecorder release];
	
    success = YES;
    
    return success;
}


//------------------------------------------------------------------------------
-(void) captureStillImage
{
    AVCaptureConnection *stillImageConnection =
    [[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo];
    
    if ([stillImageConnection isVideoOrientationSupported])
        [stillImageConnection setVideoOrientation:_orientation];
    
    [[self stillImageOutput]
     captureStillImageAsynchronouslyFromConnection:stillImageConnection
     completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
         
         if (error) {
             if ([[self delegate] respondsToSelector:@selector(captureManager:didFailWithError:)]) {
                 [[self delegate] captureManager:self didFailWithError:error];
             }
         }
         
         if (imageDataSampleBuffer != NULL) {
             NSData *imageData =
             [AVCaptureStillImageOutput
              jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
             
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             
             // TODO:  check if need to run this in main dispatch_queue
             if ([[self delegate] respondsToSelector:@selector(captureManager:captureStillImage:)]) {
                 [[self delegate] captureManager:self captureStillImage:image];
             }
         }
     }];
}


//------------------------------------------------------------------------------
-(BOOL) toggleCamera
{
    BOOL success = NO;
    
    if ([self cameraCount] > 1) {
        NSError *error;
        AVCaptureDeviceInput *newVideoInput;
        AVCaptureDevicePosition position = [[_videoInput device] position];
        
        if (position == AVCaptureDevicePositionBack)
            newVideoInput = [[AVCaptureDeviceInput alloc]
                             initWithDevice:[self frontFacingCamera]
                             error:&error];
        
        else if (position == AVCaptureDevicePositionFront)
            newVideoInput = [[AVCaptureDeviceInput alloc]
                             initWithDevice:[self backFacingCamera]
                             error:&error];
        else
            goto bail;
        
        if (newVideoInput != nil) {
            [[self session] beginConfiguration];
            [[self session] removeInput:[self videoInput]];
            if ([[self session] canAddInput:newVideoInput]) {
                [[self session] addInput:newVideoInput];
                [self setVideoInput:newVideoInput];
            } else {
                [[self session] addInput:[self videoInput]];
            }
            [[self session] commitConfiguration];
            success = YES;            
        } else if (error) {
            if ([[self delegate] respondsToSelector:@selector(captureManager:didFailWithError:)]) {
                [[self delegate] captureManager:self didFailWithError:error];
            }
        }
    }
    
bail:
    return success;

}
//------------------------------------------------------------------------------

#pragma mark Device Counts
- (NSUInteger) cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}
//------------------------------------------------------------------------------
- (NSUInteger) micCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] count];
}
//------------------------------------------------------------------------------
#pragma mark Camera Properties
// Perform an auto focus at the specified point. The focus mode will automatically change to locked once the auto focus is complete.
- (void) autoFocusAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isFocusPointOfInterestSupported] &&
        [device isFocusModeSupported:AVCaptureFocusModeAutoFocus])
    {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFocusPointOfInterest:point];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            [device unlockForConfiguration];
        } else {
            if ([[self delegate] respondsToSelector:@selector(captureManager:didFailWithError:)]) {
                [[self delegate] captureManager:self didFailWithError:error];
            }
        }
    }
}
//------------------------------------------------------------------------------
// Switch to continuous auto focus mode at the specified point
- (void) continuousFocusAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = [[self videoInput] device];
	
    if ([device isFocusPointOfInterestSupported] &&
        [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
    {
		NSError *error;
		if ([device lockForConfiguration:&error]) {
			[device setFocusPointOfInterest:point];
			[device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
			[device unlockForConfiguration];
		} else {
			if ([[self delegate] respondsToSelector:@selector(captureManager:didFailWithError:)]) {
                [[self delegate] captureManager:self didFailWithError:error];
			}
		}
	}
}
@end



//==============================================================================
//==============================================================================
//==============================================================================
@implementation IACaptureManager (InternalUtility)
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice* device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

//------------------------------------------------------------------------------
- (AVCaptureDevice *) frontFacingCamera
{
    return [self cameraWithPosition:(AVCaptureDevicePositionFront)];
}
//------------------------------------------------------------------------------
- (AVCaptureDevice *) backFacingCamera
{
    return [self cameraWithPosition:(AVCaptureDevicePositionBack)];
}
//------------------------------------------------------------------------------
- (AVCaptureDevice *) audioDevice;
{
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] > 0) {
        return [devices objectAtIndex:0];
    }
}

//------------------------------------------------------------------------------
- (NSURL *) tempFileURL
{
    return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@",
                                   NSTemporaryDirectory(), @"output.mov"]];
}
//------------------------------------------------------------------------------
- (void)deviceOrientationDidChange
{
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    
	if (deviceOrientation == UIDeviceOrientationPortrait)
		_orientation = AVCaptureVideoOrientationPortrait;
	else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown)
		_orientation = AVCaptureVideoOrientationPortraitUpsideDown;
	
	// AVCapture and UIDevice have opposite meanings for landscape left and right (AVCapture orientation is the same as UIInterfaceOrientation)
	else if (deviceOrientation == UIDeviceOrientationLandscapeLeft)
		_orientation = AVCaptureVideoOrientationLandscapeRight;
	else if (deviceOrientation == UIDeviceOrientationLandscapeRight)
		_orientation = AVCaptureVideoOrientationLandscapeLeft;
	
	// Ignore device orientations for which there is no corresponding still image orientation (e.g. UIDeviceOrientationFaceUp)
}
@end


