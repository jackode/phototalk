//
//  IALocationServiceManager.m
//  PhotoTalk
//
//  Created by Admin on 4/1/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "IALocationServiceManager.h"
#import <CoreLocation/CoreLocation.h>
@interface IALocationServiceManager()
<CLLocationManagerDelegate>
{
    CLLocationManager* locationManager;
    CLLocation* bestEffortLocation;
    IALocationServiceHandleBlock callback;
}
@end

@implementation IALocationServiceManager

//+ (IALocationServiceManager*) sharedInstance
//{
//    static IALocationServiceManager* _shareInstance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _shareInstance = [[IALocationServiceManager alloc] init];
//        
//    });
//    
//    return _shareInstance;
//}
- (id) init
{
    self = [super init];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
    }
    return self;
}

//------------------------------------------------------------------------------
- (void) currentLocationWithTimeOut: (NSTimeInterval) timeout
                    desiredAccuracy: (CLLocationAccuracy) accuracy
                           callback: (IALocationServiceHandleBlock) handler
{
    locationManager.desiredAccuracy = accuracy;
    [locationManager startUpdatingLocation];
    callback = handler;
    
    [self performSelector:@selector(stopUpdatingLocation)
               withObject:nil
               afterDelay:timeout];

}



//------------------------------------------------------------------------------
- (void) currentLocationWithTimeOut: (NSTimeInterval) timeout
                    desiredAccuracy: (CLLocationAccuracy) accuracy
                           delegate: (id<IALocationServiceDelegate>) delegate
{
    
}

//------------------------------------------------------------------------------
- (void)stopUpdatingLocation
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
}

//------------------------------------------------------------------------------
#pragma mark CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    callback(newLocation, nil);
    
}

//------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    callback(nil, error);
}
//------------------------------------------------------------------------------
@end


