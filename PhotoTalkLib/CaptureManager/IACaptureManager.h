//
//  IACaptureManager.h
//  avtest
//
//  Created by Admin on 2/5/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol IACaptureManagerDelegate;

//==============================================================================
@interface IACaptureManager : NSObject

@property (nonatomic, strong)   AVCaptureSession*           session;
@property (nonatomic        )   AVCaptureVideoOrientation   orientation;
@property (nonatomic, strong)   AVCaptureDeviceInput*       videoInput;
@property (nonatomic, strong)   AVCaptureDeviceInput*       audioInput;
@property (nonatomic, strong)   AVCaptureStillImageOutput*  stillImageOutput;

//@property (nonatomic        )   AVCaptureFlashMode          flashMode;

@property (nonatomic, weak)   id deviceConnectedObserver;
@property (nonatomic, weak)   id deviceDisconnectedObserver;

@property (nonatomic)       UIBackgroundTaskIdentifier  backgroundRecordingID;
@property (nonatomic, weak) id <IACaptureManagerDelegate> delegate;


-(BOOL) setupSession;
-(void) captureStillImage;
-(BOOL) toggleCamera;
-(NSUInteger) cameraCount;

- (void) autoFocusAtPoint:(CGPoint)point;
- (void) continuousFocusAtPoint:(CGPoint)point;
@end



//==============================================================================
@protocol IACaptureManagerDelegate <NSObject>
@optional
- (void) captureManager:(IACaptureManager *)captureManager
       didFailWithError:(NSError *)error;

- (void) captureManagerRecordingBegan:(IACaptureManager *)captureManager;
- (void) captureManagerRecordingFinished:(IACaptureManager *)captureManager;

- (void) captureManager: (IACaptureManager *)captureManager
      captureStillImage: (UIImage*) image;
- (void) captureManagerDeviceConfigurationChanged:(IACaptureManager *)captureManager;


@end
