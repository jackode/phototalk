//
//  IALocationServiceManager.h
//  PhotoTalk
//
//  Created by Admin on 4/1/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
//@protocol IALocationServiceDelegate <NSObject>
//- (void)
//
//@end

typedef void (^IALocationServiceHandleBlock)(CLLocation* location, NSError* error) ;

//------------------------------------------------------------------------------
@class IALocationServiceManager;
@protocol IALocationServiceDelegate <NSObject>

@optional
- (void) locationService: (IALocationServiceManager*) service
      didAcquireLocation: (CLLocation*) location;

@end

//------------------------------------------------------------------------------
@interface IALocationServiceManager : NSObject

// + (IALocationServiceManager*) sharedInstance;

- (void) currentLocationWithTimeOut: (NSTimeInterval) timeout
                    desiredAccuracy: (CLLocationAccuracy) accuracy
                           callback: (IALocationServiceHandleBlock) handler;

- (void) currentLocationWithTimeOut: (NSTimeInterval) timeout
                    desiredAccuracy: (CLLocationAccuracy) accuracy
                           delegate: (id<IALocationServiceDelegate>) delegate;
@end
