//
//  IAPostProcessViewController.h
//  PhotoTalk
//
//  Created by Admin on 4/1/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface IAPostProcessViewController : UIViewController
- initWithImage: (UIImage*) image audioPath: (NSString*) path;
@property (nonatomic, weak) UIImage* image;
@property (nonatomic, weak) NSString* audioPath;
@property (nonatomic, weak) CLLocation* location;
@property (nonatomic, weak) UIAcceleration* accel;
@end
