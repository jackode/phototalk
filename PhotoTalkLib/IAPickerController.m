//
//  IAViewController.m
//  PhotoTalk
//
//  Created by Admin on 1/22/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "IAPickerController.h"
#import "IAProtocols.h"
#import "IACompoundPickerViewController.h"
#import "IAPostProcessViewController.h"
#import "IALocationServiceManager.h"
#import "UIAlertView+MKBlockAdditions.h"

#define kIAPickerAudioMaximumDuration 30.0f // default 30 seconds
//==============================================================================
@interface IAPickerController ()
<IAMediaPickerDelegate>
{
#pragma mark Ivar for public interface
    IAPickerSourceType _audioSourceType;
    IAPickerSourceType _photoSourceType;
    BOOL _allowsAudioEditting;
    BOOL _allowsPhotoEditting;
    
    IAPickerQualityType _imageQuality;
    NSTimeInterval _audioMaximumDuration;
    CGAffineTransform _cameraViewTransform;
    
    // Probably unused
    NSArray* __unused _mediaTypes;
    BOOL __unused _showsCameraControls;
    UIView* __unused _cameraOverlayView;
    
    
    IAPickerCameraDevice _cameraDevice;
    IAPickerCameraFlashMode _cameraFlashMode;
    
    
#pragma mark Ivar for internal implementation
    UIViewController* _rootViewController;
    
    CLLocation* _currentLocation;
    IAPostProcessViewController* _postProcessing;
    UIAcceleration* _acceleration;
}

@end

#pragma mark - IAPickerController
//==============================================================================
@implementation IAPickerController
//------------------------------------------------------------------------------
@synthesize allowsAudioEditing=_allowsAudioEditting,
allowsImageEditing=_allowsPhotoEditting,
imageQuality=_imageQuality,
audioMaximumDuration=_audioMaximumDuration,
cameraOverlayView=_cameraOverlayView,
cameraViewTransform=_cameraViewTransform,
cameraDevice=_cameraDevice,
cameraFlashMode=_cameraFlashMode,
delegate;

//------------------------------------------------------------------------------
- (IAPickerSourceType) photoSourceType { return _photoSourceType; }

- (void) setPhotoSourceType:(IAPickerSourceType)photoSourceType {
    _photoSourceType = photoSourceType;
    [self updateRootView];
}
//------------------------------------------------------------------------------

- (IAPickerSourceType) audioSourceType { return _audioSourceType;}

- (void) setAudioSourceType:(IAPickerSourceType)audioSourceType
{
    _audioSourceType = audioSourceType;
    [self updateRootView];
}


//------------------------------------------------------------------------------
- (void) setCameraViewTransform:(CGAffineTransform)cameraViewTransform
{
    _cameraViewTransform = cameraViewTransform;
    // TODO: reset camera preview transform
}


//------------------------------------------------------------------------------
- (void) setCameraDevice:(IAPickerCameraDevice)cameraDevice
{
    // TODO: check if new camera is set, then update preview
    _cameraDevice = cameraDevice;
}

- (void) setCameraFlashMode:(IAPickerCameraFlashMode)cameraFlashMode
{
    // TODO: check if new flash mode is set, then update camera configuration
    _cameraFlashMode = cameraFlashMode;
}

- (void) setCameraOverlayView:(UIView *)cameraOverlayView
{
    _cameraOverlayView = cameraOverlayView;
    //[self updateRootView];
}

//------------------------------------------------------------------------------
- (id) init
{
    self = [super init];
    if (self) {
        [self setDefaultValue];
    }
    return self;
}

//------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Load root view for ) navigationcontroller (self)
    [self loadRootView];
}
//------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Class's methods
//------------------------------------------------------------------------------
+ (BOOL) isSourceTypeAvailable:(IAPickerSourceType) sourceType
{
    // TODO: implementation
    return NO;
}

//------------------------------------------------------------------------------
+ (NSArray*) availableMediaTypesforSourceType:(IAPickerSourceType) sourceType
{
    // TODO: implementation
    return nil;
}

//------------------------------------------------------------------------------
+ (BOOL)isCameraDeviceAvailable:(IAPickerCameraDevice)cameraDevice
{
    // TODO: implementation
    return NO;
}

//------------------------------------------------------------------------------
+ (BOOL)isFlashAvailableForCameraDevice:(IAPickerCameraDevice)cameraDevice
{
    // TODO: implementation
    return NO;
}
//------------------------------------------------------------------------------
+ (NSArray *)availableCaptureModesForCameraDevice:(IAPickerCameraDevice)cameraDevice
{
    // TODO: implementation
    return nil;
}

//------------------------------------------------------------------------------

#pragma mark - Private utilities
- (void) setDefaultValue
{
    _photoSourceType = IAPickerSourceTypeCamera;
    _audioSourceType = IAPickerSourceTypeMic;
    _allowsAudioEditting=NO;
    _allowsPhotoEditting=NO;
    
    _imageQuality = IAPickerQualityTypeMedium;
    _audioMaximumDuration = 30.0f;
    _mediaTypes = nil;
    _showsCameraControls = YES;
    _cameraOverlayView = nil;
    _cameraViewTransform  = CGAffineTransformIdentity;
    
    _cameraDevice = IAPickerCameraDeviceCameraDeviceRear;
    _cameraFlashMode = IAPickerCameraFlashModeAuto;
}

//------------------------------------------------------------------------------
- (void) loadRootView
{
#warning need implementation
    // Create root view according to current setting
    if ((_photoSourceType == IAPickerSourceTypeCamera) &&
        _audioSourceType == IAPickerSourceTypeMic)
    {
        // Create compound photo/audio picker from device.
        IACompoundPickerViewController* compoundPicker =
        [[IACompoundPickerViewController alloc]
         initWithCameraType:_cameraDevice
         imageQuality:_imageQuality
         audioDuration:_audioMaximumDuration
         delegate:self];
        
        
        // Add view as root view controller of navigation view controller's
        // stack
        NSArray* viewControllers = [NSArray arrayWithObject:compoundPicker];
        [self setViewControllers:viewControllers];
        [self setNavigationBarHidden:YES];
        
    }else if((_photoSourceType == IAPickerSourceTypeCamera) &&
             _audioSourceType == IAPickerSourceTypeAudioLibrary){
        // Create photo picker (to capture image only)
    }else if((_photoSourceType == IAPickerSourceTypePhotoLibrary)&&
             (_audioSourceType == IAPickerSourceTypeMic)){
        // Create photo picker (to pick image from library)
    }else if((_photoSourceType == IAPickerSourceTypePhotoLibrary)&&
             (_audioSourceType == IAPickerSourceTypeAudioLibrary))
    {
        // Create photo picker (to pick image from library) 
    }
    
}

//------------------------------------------------------------------------------
/*!
 This will be a big ass function that will create/update root view of this 
 picker (a navigation controller). What instance of root view is created depend
 on what source type of audio and photo.
 */
- (void) updateRootView
{
    
    return;
    
    if ((_audioSourceType == IAPickerSourceTypeMic) &&
        (_photoSourceType == IAPickerSourceTypeCamera))
    {
        
        // Create compound photo/audio picker from device.
        IACompoundPickerViewController* compoundPicker =
        [[IACompoundPickerViewController alloc]
         initWithCameraType: _cameraDevice
         imageQuality: _imageQuality
         audioDuration: _audioMaximumDuration
         delegate: self];
        
        // Add view as root view controller of navigation view controller's
        // stack
        NSArray* viewControllers = [NSArray arrayWithObject:compoundPicker];
        [self setViewControllers:viewControllers];
    }
}

//------------------------------------------------------------------------------
#pragma mark - Picker delegate methods
- (void) picker: (id) picker
 didPickedImage: (UIImage*) image
      audioPath: (NSString *) audioPath
   acceleration: (UIAcceleration *)acceleration
{
    // TODO: determine type of picker
    // Create postprocess controller, push to navigator
    
    _acceleration = acceleration;
    
    /// For now, only compound picker is implemented

    _postProcessing = nil;
    //if (!_postProcessing) {
        _postProcessing =  [[IAPostProcessViewController alloc]
                            initWithImage:image
                            audioPath:audioPath];
//    }else{
//        [_postProcessing setImage:image];
//        [_postProcessing setAudioPath:audioPath];
//    }   
    
    sleep(1);
    [self setNavigationBarHidden:NO animated:YES];
    [self pushViewController:_postProcessing animated:YES];

    
    [_postProcessing setAccel:_acceleration];
    IALocationServiceManager* locationManager = [[IALocationServiceManager alloc] init];
    
    [locationManager
     currentLocationWithTimeOut:10
     desiredAccuracy: 100
     callback:^(CLLocation *location, NSError *error) {
         if (error) {
             [UIAlertView
              alertViewWithTitle:@"Location Service"
              message:@"Can not acquired location"
              cancelButtonTitle:@"Dismiss"
              onCancel:nil];
             return;
         }
         
         _currentLocation = location;
         [_postProcessing setLocation:_currentLocation];
     }];
}

- (void) picker: (id) picker
failedWithError: (NSError*) error
{
    // TODO: determine type of picker
    // Handler error
    
    /// Temp: handle case that device init failed
    if ([delegate respondsToSelector:@selector(iaPicker:didCancelWithError:)]) {
        [delegate iaPicker:self didCancelWithError:error];
    }
}

//------------------------------------------------------------------------------
#pragma mark - Editor delegate methods

@end
