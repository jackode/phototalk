//
//  IACompoundPickerViewController.h
//  PhotoTalk
//
//  Created by Admin on 1/29/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPickerController.h"


@protocol IAMediaPickerDelegate;
//------------------------------------------------------------------------------

@interface IACompoundPickerViewController : UIViewController
- (id)initWithCameraType:(IAPickerCameraDevice)deviceType
            imageQuality:(IAPickerQualityType)imageQuality
           audioDuration:(NSTimeInterval)audioDuration
                delegate:(id <IAMediaPickerDelegate>)delegate;
@end
