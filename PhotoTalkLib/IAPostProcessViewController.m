//
//  IAPostProcessViewController.m
//  PhotoTalk
//
//  Created by Admin on 4/1/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import "IAPostProcessViewController.h"
#import <QuartzCore/CALayer.h>
#import <AVFoundation/AVFoundation.h>
#import "CLLocation (Strings).h"
@interface IAPostProcessViewController ()
{
    IBOutlet UILabel* _lbLocation;
    IBOutlet UILabel* _lbAccelerometer;
    IBOutlet UIImageView* _imageView;
    
    
    AVAudioPlayer* audioPlayer;
}
@end

//------------------------------------------------------------------------------
@implementation IAPostProcessViewController
@synthesize image=_image;
@synthesize audioPath=_audioPath;
@synthesize location=_location;
@synthesize accel=_accel;
- (void) setLocation:(CLLocation *)location
{
    _location = location;
    _lbLocation.text = [NSString stringWithFormat:@"Location:\n%@",[location localizedCoordinateString]]; 
}

-(void) setAccel:(UIAcceleration *)accel
{
    _accel =accel;
    _lbAccelerometer.text = [NSString
                             stringWithFormat:@"Camera pose:\nx=%.3f,y=%.3f,z=%.3f",
                             _accel.x, _accel.y, _accel.z];
}

//------------------------------------------------------------------------------
- (id) initWithImage:(UIImage *)image
           audioPath:(NSString *)path
{
    self = [super initWithNibName:@"IAPostProcessViewController"
                           bundle:nil];
    if (self) {
        _image = image;
        _audioPath = path;
    }
    
    return self;
}

//------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[_lbLocation.layer setCornerRadius:10];
    //[_lbAccelerometer.layer setCornerRadius:10];
    [_imageView setImage:_image];    
}

//------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)playBackRecord:(id)sender {
    audioPlayer = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:[NSURL fileURLWithPath:_audioPath]
                   error:nil];
    [audioPlayer play];
}


@end
