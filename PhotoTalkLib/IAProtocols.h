//
//  IAProtocols.h
//  PhotoTalk
//
//  Created by Admin on 1/29/13.
//  Copyright (c) 2013 Jcorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IAMediaPickerDelegate <NSObject>
- (void) picker: (id) picker
 didPickedImage: (UIImage*) image
      audioPath: (NSString *) audioPath
   acceleration: (UIAcceleration*) acceleration;

- (void) picker: (id) picker
failedWithError: (NSError*) error;

@end

////------------------------------------------------------------------------------
//@protocol IAPhotoPickerDelegate <IAMediaPickerDelegate>
//
//@end
//
////------------------------------------------------------------------------------
//@protocol IAAudioPickerDelegate <IAMediaPickerDelegate>
//- (void) picker: (id) picker didRecordAudioToPath: (NSString*) path;
//- (void) picker:(id)picker failedToRecordAudio: (NSError*) error;
//
//@end

//------------------------------------------------------------------------------
@protocol IAMediaPicker <NSObject>
@property (nonatomic, weak) id  delegate;
@end

//------------------------------------------------------------------------------
@protocol IAAudioPicker <IAMediaPicker>
@end

//------------------------------------------------------------------------------
@protocol IAPhotoPicker <IAMediaPicker>
@end



//==============================================================================

@protocol IAMediaEditorDelegate <NSObject>

@end

//------------------------------------------------------------------------------
@protocol IAAudioEditorDelegate <IAMediaEditorDelegate>

@end

//------------------------------------------------------------------------------
@protocol IAPhotoEditorDelegate <IAMediaEditorDelegate>

@end
    
//------------------------------------------------------------------------------
@protocol IAMediaEditor <NSObject>

@end

//------------------------------------------------------------------------------
@protocol IAAudioEditor <IAMediaEditor>
@end

//------------------------------------------------------------------------------
@protocol IAPhotoEditor <IAMediaEditor>
@end




